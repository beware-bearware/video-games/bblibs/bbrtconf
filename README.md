# bbRTConf

! bbRTConf IS NOT READY FOR GENERAL USE !

bbRTConf is an autoload script that can be dropped into a Godot project to allow for a quick and easy way to create client settings that can have their values changed and checked through out the project. Adding new settings is easy, and can be combined with bbGUI to make a menu system that allows the user to change the settings easily.

## Installation

If you don't already have one, create a folder at the root of your game project named bbLibs, then go into that directory and clone this repository.

```bash
git clone https://gitlab.com/beware-bearware/video-games/bblibs/rtconf.git
```

Then activate it by going to the Project tab in the upper left of the editor, and going into Project Settings > Autoload. Set the path to res://bbLibs/RTConf/bbRTConf.gd and the name to bbRTConf, then press add. bbRTConf is now usable in your code!

## Usage

Due to being autoloaded, the script can be called from any other script by using bbRTConf. All the settings are stored in an array which can be accessed directly, but this should be avoided.

```gdscript
#TODO
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
